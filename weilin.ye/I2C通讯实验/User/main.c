/**
  ******************************************************************************
  * @file    main.c
  * @author  fire
  * @version V1.0
  * @date    2013-xx-xx
  * @brief   串口中断接收测试
  ******************************************************************************
  * @attention
  *
  * 实验平台:秉火 F103-霸道 STM32 开发板 
  * 论坛    :http://www.firebbs.cn
  * 淘宝    :https://fire-stm32.taobao.com
  *
  ******************************************************************************
  */ 
 
 
#include "stm32f10x.h"
#include "bsp_usart.h"
#include "./i2c/bsp_i2c.h"

//1.初始化IIC相关的GPIO
//1.配置IIC外设的工作模式
//3.编写IIC写入EEPROM的Byte write大维函数
//4.编写IIC读取EEPROM的RANDOM Read函数
//5.实验read函数和write函数进行读写校验
//6.编写page write 及 seq read函数并校验

/**
  * @brief  主函数
  * @param  无
  * @retval 无
  */
	uint8_t i=0;
	uint8_t  readData[10] = {0};
	uint8_t  writeData[8] = {4,5,6,7,8,9,10,11};
int main(void)
{	
  /*初始化USART 配置模式为 115200 8-N-1，中断接收*/
  USART_Config();
	
	/* 发送一个字符串 */
//	Usart_SendString( DEBUG_USARTx,"这是一个串口中断接收回显实验\n");
	printf("这是一个I2C通讯实验\n");

	//初始化IIC
	I2C_EE_Config();
	
	//写入一个字节
	EEPROM_Byte_Write(11,55);
	//等待写入操作完成
	EEPROM_WaitForWriteEnd();
	//addr%8 == 0,即为地址对齐
	EEPROM_Page_Write(8,writeData,8);
  EEPROM_WaitForWriteEnd();
	
	//读取数据
	EEPROM_Read(8,readData,8);

	
	for(i=0;i<8;i++)
	{
		printf("%d ",readData[i]);
	}
	
//	 printf("\r\接收到的数据为0x%x\r\n",readData[0]);
	
  while(1)
	{	
		
	}	
}




/*********************************************END OF FILE**********************/
