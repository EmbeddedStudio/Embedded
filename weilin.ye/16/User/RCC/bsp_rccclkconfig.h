#ifndef _BSP_RCCCLKCONFIG_H
#define _BSP_RCCCLKCONFIG_H

#include "stm32f10x.h"

void HSE_SetSysClk(uint32_t RCC_PLLMul_x);
void MCO_GPIO_Config();
void HSI_SetSysClk(uint32_t RCC_PLLMul_x);

#endif/*_BSP_RCCCLKCONFIG_H*/



