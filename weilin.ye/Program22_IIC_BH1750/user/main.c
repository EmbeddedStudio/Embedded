/****
Notice：
以下已经用三种方法实现了stm32 BH1750实验，但是其中依然有些地方没有真正弄明白，
（1）比如IIC.c文件中，IIC_Send_Byte()函数中有四句话，有或者没有会对程序造成很大的影响。
（2）读书只能读到54612.5lux,读不到65535，这是因为读完数据后除以1.2导致的。
****/

#include "usart.h"
#include "sys.h"
#include "delay.h"
#include "stm32f10x.h"
#include "IIC.h"

#include "BH1750.h"


extern float lux;


int main(void)
{
  //float data;
	delay_init();	    	 //延时函数初始化	  
	uart_init(115200);
	BH1750_Init();
	
	
	
	
		/****方法一：使用此while循环****/
while(1)
{

	  //Single_Write_BH1750(0x07);    //Power On
//	  BH1750_Multiple_Read();
	  BH1750_ReadContinuous1();
 		delay_ms(200);


		printf("light:");
		printf("%5.1f \r",lux);   
		printf("lux\r\n");    //注意此处的lux仅仅代表单位
		delay_ms(500);
		delay_ms(500);
	
}
	
	
	
	
	
	/****方法二：使用此while循环****/
// while(1)
// {

// 	  //Single_Write_BH1750(0x07);    //Power On
// 	  BH1750_ReadContinuous2();
//  		delay_ms(200);
//  		Conversion();	  

// 		printf("light:");
// 		printf("%5.1f \r",lux);   
// 		printf("lux\r\n");    //注意此处的lux仅仅代表单位
// 		delay_ms(500);
// 		delay_ms(500);
// 	
// }

	
	
	
	
	
	
	//方法三时使用此while循环
// 	while(1)
// 	{
// 		Single_Write_BH1750(0x01);    //Power On
// 		Single_Write_BH1750(0x10);    //连续H-Mode
// 		
// 		BH1750_Multiple_Read();
// 		delay_ms(200);
// 		Conversion();
// 		printf("light:");
// 		printf("%5.1f \r",lux);   
// 		printf("lux\r\n");    //注意此处的lux仅仅代表单位
// 		delay_ms(500);
// 		delay_ms(500);
// 	}
// 	
	
}







