#ifndef  __KEY_H
#define __KEY_H
#include "sys.h"
#define KEY0 GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_4)  //读取指定端口管教的输入
#define KEY1 GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_3)
#define KEY2 GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_2)
#define KEY3 GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0)

#define KEY_UP 4
#define KEY_LEFT 3
#define KEY_DOWN 2
#define KEY_RIGHT 1
void KEY_Init(void);
u8 KEY_Scan(u8);
#endif






