/****
本软件包包含了IIC直接面对器件的操作函数
                                                                             ------更新时间2018.2.23
****/
#include "24cxx.h"
#include "delay.h"
#include "IIC.h"


/****初始化IIC接口****/
void AT24cxx_Init(void)
{
	IIC_Config();
}


/****在AT24cxx指定地址读出一个数据****/
/****
SlaveAddr：器件子地址
返回值：读到的数据
****/
u8 AT24cxx_ReadOneByte(u8 ReadSlaveAddr)
{
	u8 temp=0;
	IIC_Start();
	IIC_Send_Byte(0xA0);  //发送器件地址A0
	IIC_Wait_Ack();
	IIC_Send_Byte(ReadSlaveAddr);  //发送器件子地址
	IIC_Wait_Ack();
	IIC_Start();
	IIC_Send_Byte(0xA1);  //发送器件地址A1,1表示读，即进入接收模式
	IIC_Wait_Ack();	
	temp=IIC_Read_Byte(0);   //0表示不产生应答
	IIC_Stop();
	return temp;

}

/****在AT24cxx指定地址写入一个数据****/
/****
SlaveAddr：要写入的地址
DataToWrite:要写入的数据
****/
void AT24cxx_WriteOneByte(u8 WriteSlaveAddr,u8 DataToWrite)
{
	IIC_Start();
	IIC_Send_Byte(0xA0);  //发送器件地址0xA0，24c02
	IIC_Wait_Ack();
	IIC_Send_Byte(WriteSlaveAddr);  //发送器件子地址
	IIC_Wait_Ack();
  IIC_Send_Byte(DataToWrite);  //发送一个字节
	IIC_Wait_Ack();
  IIC_Stop();
  delay_ms(10);
}

/****在24cxx指定的地址开始读出指定个数的数据****/
/****
SlaveAddr：开始读出的地址，24c02对应0~255
pBuffer:数据数组首地址
NumToRead:要读出数据的个数
注意：a++,是后置++，即执行完语句后，a的值才会加1
例子：*p++=a[i];  //将a[i]的值赋值给p指针指向的目标，然后再p=p+1;
****/
void AT24cxx_Read(u8 ReadSlaveAddr,u8 *pBuffer,u8 NumToRead)   //定义函数，形参为字符指针变量
{
	while(NumToRead)
	{
		*pBuffer++=AT24cxx_ReadOneByte(ReadSlaveAddr++);  //
		NumToRead--;
	}
}

/****在24cxx指定的地址开始写入指定个数的数据****/
/****


****/
void AT24cxx_Write(u8 WriteSlaveAddr,u8 *pBuffer,u8 NumToWrite)   //定义函数，形参为字符指针变量
{
	while(NumToWrite--)
	{
		AT24cxx_WriteOneByte(WriteSlaveAddr,*pBuffer);
		WriteSlaveAddr++;
		pBuffer++;
	}

}






/***向有子地址器件发送多字节数据函数***/
/***即向从器件写数据***/
/***
功能：从启动总线到发送地址、子地址、写数据、结束总线。
从器件地址Slave_Addr，子地址Slave_SubAddr，发送内容是s指向的内容，发送Num个字节。
如果返回1表示操作成功，否则操作有误

注意:使用前必须已结束总线

思考：为什么要用指针*s来表示数据呢？

***/


// u8 AT24cxx_WriteStr(unsigned char Slave_Addr,unsigned char Slave_SubAddr,unsigned char *s,unsigned char Num)
// {
// 	unsigned char i;
// 	IIC_Start();       //启动总线
// 	IIC_SendByte(Slave_Addr);    //发送器件地址
// 	if(IIC_ack==0)   return 0;
// 	IIC_SendByte(Slave_Addr);    //发送器件子地址
// 	if(IIC_ack==0) return 0;
// 	
// 	for(i=0;i<Num;i++)
// 	{
// 		IIC_SendByte(*s);     //发送数据
// 		if(IIC_ack==0) return 0;
// 		s++;
//    }
// 	IIC_Stop();      //结束总线
// 	 return 1;
// }

/***向有子地址器件读取多字节数据函数***/
/***即从从器件读数据***/
/***功能：从启动总线到发送地址、子地址、读数据、结束总线。
从器件地址Slave_Addr，子地址Slave_SubAddr，读出的内容放入s指向的存储区，读Num个字节。
如果返回1表示操作成功，否则操作有误

注意:使用前必须已结束总线

***/
// u8 AT24cxx_ReadStr(unsigned char Slave_Addr,unsigned char Slave_SubAddr,unsigned char *s,unsigned char Num)
// {
// 	unsigned char i;
// 	IIC_Start();       //启动总线
// 	IIC_SendByte(Slave_Addr);    //发送器件地址
// 	if(IIC_ack==0)   return 0;
// 	IIC_SendByte(Slave_Addr);    //发送器件子地址
// 	if(IIC_ack==0) return 0;
// 	
// 		IIC_Start();       //启动总线
// 	IIC_SendByte(Slave_Addr+1);     //此处疑问？为什么要加1呢??????????????
// 	if(IIC_ack==0) return 0;
// 	for(i=0;i<Num-1;i++)
// 	{
// 		*s=IIC_RecByte();    //发送数据
// 		IIC_Ack(0);         //发送应答位
// 		s++;
//    }
// 	 *s=IIC_RecByte();
// 		IIC_Ack(1);         //发送非应答位	 
// 		IIC_Stop();      //结束总线
// 	 return 1;
// }





