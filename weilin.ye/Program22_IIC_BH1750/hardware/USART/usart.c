#include "usart.h"
#include "stm32f10x_usart.h"
#include "sys.h"

/*-----------------主函数中需要定义一个数组-----用来保存接收中断时接收到的数据--------------------*/

uint8_t RS232_RX_BUF[24];//数组
uint8_t RS232_RX_CNT=0;//数组实时下标

/*------------到时候直接判断或者使用这个数组中的相对应位数据--------------------


--------------中断--------接收中断-------------------------*/

/*------先申明在主函数中定义接收数组-----位外部变量-------
                      特别注意申明外部变量时不能赋值--------------------*/

extern uint8_t RS232_RX_BUF[24];
extern uint8_t RS232_RX_CNT;



//////////////////////////////////////////////////////////////////
//加入以下代码,支持printf函数,而不需要选择use MicroLIB      
#if 1
#pragma import(__use_no_semihosting)             
//标准库需要的支持函数                 
struct __FILE 
{ 
    int handle; 

}; 

FILE __stdout;       
//定义_sys_exit()以避免使用半主机模式    
_sys_exit(int x) 
{ 
    x = x; 
} 
//重定义fputc函数 
int fputc(int ch, FILE *f)
{      
    while(USART_GetFlagStatus(USART1,USART_FLAG_TC)==RESET); 
    USART_SendData(USART1,(uint8_t)ch);   
    return ch;
}
#endif 


 
#if EN_USART1_RX   //如果使能了接收
//串口1中断服务程序
//注意,读取USARTx->SR能避免莫名其妙的错误       
u8 USART_RX_BUF[USART_REC_LEN];     //接收缓冲,最大USART_REC_LEN个字节.
//接收状态
//bit15，    接收完成标志
//bit14，    接收到0x0d
//bit13~0，    接收到的有效字节数目
u16 USART_RX_STA=0;       //接收状态标记      


void usart_config()
{

    USART_InitTypeDef USART_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;
    
    
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1|RCC_APB2Periph_GPIOA,ENABLE);
      
      USART_DeInit(USART1);
    
    
    
    //GPIO
    GPIO_InitStructure.GPIO_Pin=GPIO_Pin_9;         //USART1_TX
    GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AF_PP;    //需选择复用推挽输出
    GPIO_Init(GPIOA,&GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin=GPIO_Pin_10;       //USART1_RX
    GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode=GPIO_Mode_IN_FLOATING;   //需选择浮空输入或上拉输入
    
    GPIO_Init(GPIOA,&GPIO_InitStructure);
    
    
    //USART
    USART_InitStructure.USART_BaudRate=9600;
    USART_InitStructure.USART_WordLength=USART_WordLength_8b;
    USART_InitStructure.USART_StopBits=USART_StopBits_1;
    USART_InitStructure.USART_Parity=USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl=USART_HardwareFlowControl_None;    //硬件流控制失能
    USART_InitStructure.USART_Mode=USART_Mode_Tx | USART_Mode_Rx;    //发送使能，接收使能
    USART_Init(USART1,&USART_InitStructure);
        
        /*特别注意：串口1配置好了。但代码一运行就会发现不妥！为什么每次初始化完成就马上进入中断了呢？？？
        遇到这种现象千万不要大惊小怪，我很淡(dan)定（teng）地做了个实验，
        发现处理器复位后，串口的SR寄存器中的TC标志会被置位。而根《STM32中文参考手册》25.3.2节，
        在串口使能后会自动发送一个空闲帧，发送完毕后TC也会置位，
        所以初始化将导致串口初始化完毕后马上进入TC中断。
        为了避免这种情况，可以在串口使能后等待空闲帧发送完毕，再打开TC中断。             */
        USART_ClearFlag(USART1,USART_FLAG_TC);     //清除串口1发送中断，否则第一个数不会发生
    
    USART_Cmd(USART1,ENABLE);
        
        
        //串口使能后，会发送一个空闲帧，等待空闲帧发送完毕后将TC标志位清零
        while(USART_GetFlagStatus(USART1,USART_FLAG_TC)!=SET);
        //否则开启TC中断后会马上中断
        USART_ClearFlag(USART1,USART_FLAG_TC);
    
    
    //NVIC
    NVIC_InitStructure.NVIC_IRQChannel=USART1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority=0;
    NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
    NVIC_Init(&NVIC_InitStructure);
        

    
    USART_ITConfig(USART1,USART_IT_RXNE,ENABLE);    //接收中断
    
    
}
#endif

/****方案一：打印字符和字符串*****/
//将USART1_Putc()内容打印到串口
//打印一个字符到串口
void USART1_Putc(unsigned char c)
{
    USART_SendData(USART1,c);
    while(USART_GetFlagStatus(USART1,USART_FLAG_TXE)==RESET);  //TXE为0时，一直在while循环内，直到TXE=1，说明寄存器空，数据已经被传送到移位寄存器
}


//将USART1_Puts()内容打印到串口
//打印一个字符串到串口
void USART1_Puts(char *str)      //定义一个char型的指针变量str
{
    while(*str)     //检查指针变量str所指向的变量是否为0，即检查str所指向的地址中的数据是否为0
    {
        USART_SendData(USART1,*str++);    //最终的实际执行效果等效为先执行（*str）操作，后str自加。
        while(USART_GetFlagStatus(USART1,USART_FLAG_TXE)==RESET);
   }
     
}


/****方案二：打印字符，字符串，和数组*****/
//串口发送一个字节
void MyPrintfByte(unsigned char byte)
{
    USART_SendData(USART1,byte);      //通过库函数发送数据
    while(USART_GetFlagStatus(USART1,USART_FLAG_TC)!=SET);      //等待发送完成，检测TC是否置位
}

//发送字符串
void MyPrintfStr(unsigned char *s)
{
    uint8_t i=0;     //定义一个局部变量
    while(s[i]!='\0')        //每个字符串结尾都是以 \0 结尾的
    {
        USART_SendData(USART1,s[i]);      //通过哭函数发送数据
        while(USART_GetFlagStatus(USART1,USART_FLAG_TC)!=SET);   //等待发送完成，检测TC是否置位
        i++;
    }
}

//发送数组中指定个数
void MyPrintfArray(uint8_t send_array[],uint8_t num)       //一个数组内容，一个数组长度1~255
{
      uint8_t i=0;  //定义一个局部变量  用来 发送字符串 ++运算
         while(i<num)   //i肯定小于num 是正确  就执行
        {
            USART_SendData(USART1,send_array[i]);        //通过库函数  发送数据
         while( USART_GetFlagStatus(USART1,USART_FLAG_TC)!= SET);  //等待发送完成，检测 USART_FLAG_TC 是否置1
            i++;  //加一         
        }        

}





/****USART1中断服务程序方案一*****/

// void USART1_IRQHandler(void)
// {
//     u16 RXData;
//     if(USART_GetITStatus(USART1,USART_IT_RXNE)!=RESET)    //USART_IT_RXNE检查接收中断
//     {
//         
//         USART_ITConfig(USART1,USART_IT_RXNE,DISABLE);    //进入一个中断后，暂停接收中断  思考：如果没有禁止中断会怎样呢？
//         RXData=USART_ReceiveData(USART1);     //最近接收到的数据，来自上位机，疑问：此处需要失能接收中断吗？
//         USART_SendData(USART1,RXData);          //发送出去，即发到上位机
//         while(USART_GetFlagStatus(USART1,USART_FLAG_TXE)==RESET)
//         USART_ITConfig(USART1,USART_IT_RXNE,ENABLE);    //数据发送到上位机后，从新使能接收中断
//                                                                                        

//    }

// }




/****USART1中断服务程序方案二****/

void USART1_IRQHandler(void)
{
        uint8_t res;
         if(USART_GetITStatus(USART1,USART_IT_RXNE)!=RESET)
         {
                res =USART_ReceiveData(USART1);  //读取接收到的数据
             RS232_RX_BUF[RS232_RX_CNT]=res;  //记录接收到的值
             USART_SendData(USART1, res);//发送数据----回显
                RS232_RX_CNT++;      //接收数据增加1 
             if( RS232_RX_CNT>23)
                RS232_RX_CNT=0;  
             
             }
            if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET) 
            { 
                             USART_ClearITPendingBit(USART1, USART_IT_RXNE);//清除接收标志位------每次接收完成都需要清除一下
            }

}



//系统软件复位
void Sys_Soft_Reset(void)
{
	SCB->AIRCR = 0X05FA0000|(u32)0x04;
}



















