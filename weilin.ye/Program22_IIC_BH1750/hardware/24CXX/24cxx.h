#ifndef __24CXX_H
#define __24CXX_H

#include "sys.h"

#include "IIC.h"



void AT24cxx_Init(void);
u8 AT24cxx_ReadOneByte(u8 ReadSlaveAddr);
void AT24cxx_WriteOneByte(u8 WriteSlaveAddr,u8 DataToWrite);
void AT24cxx_Read(u8 ReadSlaveAddr,u8 *pBuffer,u8 NumToRead);
void AT24cxx_Write(u8 WriteSlaveAddr,u8 *pBuffer,u8 NumToWrite);



#endif





