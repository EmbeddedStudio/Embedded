#include "led.h"
//初始化PB5和PE5为输出口，并使能这两个口的时钟
//LED初始化


void LED_Init(void)
{

	
	 GPIO_InitTypeDef  GPIO_InitStructure;
	
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB|RCC_APB2Periph_GPIOE, ENABLE);    //使能GPIOB端口时钟
	//RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE, ENABLE);    //使能GPIOE端口时钟
	
	
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_5;
		GPIO_InitStructure.GPIO_Mode=GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed= GPIO_Speed_50MHz;

	
  GPIO_Init(GPIOB,&GPIO_InitStructure);
  GPIO_SetBits(GPIOB,GPIO_Pin_5);    //PB.5输出高，关闭LED0
	
	
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_5;
	GPIO_InitStructure.GPIO_Speed= GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_Out_PP;
	
	GPIO_Init(GPIOE,&GPIO_InitStructure);
  GPIO_SetBits(GPIOE,GPIO_Pin_5);    //PE.5输出高，关闭LED1

}




