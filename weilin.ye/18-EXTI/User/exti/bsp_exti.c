#include "bsp_exti.h"

//3.初始化NVIC,用于处理中断
 void EXTI_NVIC_Config(void)
{     //嵌套向量中断控制器
    NVIC_InitTypeDef NVIC_InitStruct;
      //配置中断优先级
	  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
      //配置NVIC初始化结构体里面的成员
      //中断线EXTI0   按键1
	  NVIC_InitStruct.NVIC_IRQChannel = EXTI0_IRQn;
      //主优先级,抢占优先级，先比较
	  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 1;
 	    //子优先级，次比较
	  NVIC_InitStruct.NVIC_IRQChannelSubPriority = 1;
	    //使能，开总中断
	  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	    //NVIC的初始化函数
	  NVIC_Init(&NVIC_InitStruct);
	
	    //按键2
	  NVIC_InitStruct.NVIC_IRQChannel = EXTI15_10_IRQn;
	    //NVIC的初始化函数
		NVIC_Init(&NVIC_InitStruct);
}


void EXTI_Key_Config(void)
{ 
	  //1.初始化要连接到EXTI 的GPIO
	GPIO_InitTypeDef GPIO_InitStruct;
	  //初始化EXTI结构体
	EXTI_InitTypeDef EXTI_InitStruct;//定义初始化结构体EXTI_InitStruct
	
	  //配置中断优先级
	EXTI_NVIC_Config();
   //初始化GPIO 
	 //按键1
	RCC_APB2PeriphClockCmd(KEY1_INT_GPIO_CLK, ENABLE);	
  GPIO_InitStruct.GPIO_Pin = KEY1_INT_GPIO_PIN;
	
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN_FLOATING;

	GPIO_Init(KEY1_INT_GPIO_PORT, &GPIO_InitStruct);
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	  //2.初始化EXTI,用于产生中断/事件
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOA,GPIO_PinSource0);
	
	EXTI_InitStruct.EXTI_Line = EXTI_Line0;//如Pin0一样
	  //配置成中断模式
	EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
	  //出发选择，如上升沿下降沿
	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;
    //中断时间寄存器使能，打开响应中断
	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStruct);
	
 	   //按键2
	RCC_APB2PeriphClockCmd(KEY2_INT_GPIO_CLK, ENABLE);
	
  GPIO_InitStruct.GPIO_Pin = KEY2_INT_GPIO_PIN;
	
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN_FLOATING;

	GPIO_Init(KEY2_INT_GPIO_PORT, &GPIO_InitStruct);
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	  //2.初始化EXTI,用于产生中断/事件
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOC,GPIO_PinSource13);
	
	EXTI_InitStruct.EXTI_Line = EXTI_Line13;//如Pin0一样
	  //配置成中断模式
	EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
	  //出发选择，如上升沿下降沿
	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Falling;
    //中断时间寄存器使能，打开响应中断
	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStruct);


}




//4.编写中断服务函数在stm32f10x_it.c中编写函数

