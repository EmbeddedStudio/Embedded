#include "stm32f10x.h"  //相当于51单片机中的 #include <reg51.h>
#include "bsp_led.h"
#include "bsp_key.h"

void delay(uint32_t time)
{
	for(;time>0;time--);
}
 
int main(void)
	
{      	
	//来到这里的时候，系统的时钟已经被配置成72M。
    LED_GPIO_Config0();
	  LED_GPIO_Config1();
	  KEY_GPIO_Config_0();
	  KEY_GPIO_Config_1();
	  while(1)
		{
			if(Key_Scan(KEY1_GPIO_PORT,KEY1_GPIO_PIN) == KEY_ON)
			{
				LED_G_TOGGLE;
			}
			else if(Key_Scan(KEY2_GPIO_PORT,KEY2_GPIO_PIN) == KEY_ON)
			{
				LED_B_TOGGLE;
			}
	}
//while(1)  //（三色闪烁）
//	{     delay(0xfffff);
//		LED_GPIO_Config_0();
//		GPIO_ResetBits(LED_G_GPIO_PORT, LED_G_GPIO_PIN);
//		  delay(0xfffff);
//		  delay(0xffffff);
//	    GPIO_SetBits(LED_G_GPIO_PORT,LED_G_GPIO_PIN);//BSRR 1->1
//		  delay(0xfffff);
//		LED_GPIO_Config1();
//		GPIO_ResetBits(LED_G_GPIO_PORT, LED_B_GPIO_PIN);
//		  delay(0xfffff);
//		  delay(0xffffff);
//	    GPIO_SetBits(LED_G_GPIO_PORT,LED_B_GPIO_PIN);
//	      delay(0xfffff);
//		LED_GPIO_Config5();
//		GPIO_ResetBits(LED_G_GPIO_PORT, LED_R_GPIO_PIN);
//          delay(0xfffff);
//		  delay(0xffffff);		
//	    GPIO_SetBits(LED_G_GPIO_PORT,LED_R_GPIO_PIN);
		
		//LED_G(NO);
	//GPIO_ResetBits(LED_G_GPIO_PORT, LED_G_GPIO_PIN);//BRR 1->0
		//LED_G(OFF);
//  }
}


