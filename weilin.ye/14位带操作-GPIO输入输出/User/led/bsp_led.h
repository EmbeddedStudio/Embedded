#ifndef _BSP_LED_H
#define _BSP_LED_H

#include "stm32f10x.h"

#define  LED_G_GPIO_PIN       GPIO_Pin_0
#define  LED_G_GPIO_PORT      GPIOB
#define  LED_G_GPIO_CLK       RCC_APB2Periph_GPIOB
#define  LED_R_GPIO_PIN       GPIO_Pin_5
#define  LED_B_GPIO_PIN       GPIO_Pin_1
#define  NO      1
#define  OFF     0
// \ C语言里面叫续行符，后面不能有任何的东西
#define  LED_G(a)      if(a)  \
	                        GPIO_ResetBits(LED_G_GPIO_PORT, LED_G_GPIO_PIN);\
                       else GPIO_SetBits(LED_G_GPIO_PORT,  LED_G_GPIO_PIN);
//  ^异或符号，C语言的一个二进制的运算符
//  与1异或改变（0与1异或变成1，1与1异或变成0），与0异或不变
#define LED_G_TOGGLE   {LED_G_GPIO_PORT->ODR ^=LED_G_GPIO_PIN;}
#define LED_B_TOGGLE   {LED_G_GPIO_PORT->ODR ^=LED_B_GPIO_PIN;}

void LED_GPIO_Config_0(void );   
void LED_GPIO_Config1(void );
void LED_GPIO_Config5(void );
#endif /*_BSP_LED_H*/


