#include "bsp_systick.h"

#if 0

/*系统定时器配置库函数*/
static __INLINE uint32_t SysTick_Config(uint32_t ticks)
{ 
	//判断tick的值是否大于2^24，如果大于，则违法的（不符合规则）
  if (ticks > SysTick_LOAD_RELOAD_Msk)  return (1); 
     
   //初始化reload寄存器的值	
  SysTick->LOAD  = (ticks & SysTick_LOAD_RELOAD_Msk) - 1; 
  
	//配置中断优先级，（1<<4-1）配置为15，默认为最低的优先级
	NVIC_SetPriority (SysTick_IRQn, (1<<__NVIC_PRIO_BITS) - 1); 
  
	//初始化counter（计数器）的值为0
	SysTick->VAL   = 0;
  
	//配置systick的时钟为 72M
	//使能中断
	//使能systick
	SysTick->CTRL  = SysTick_CTRL_CLKSOURCE_Msk | 
                   SysTick_CTRL_TICKINT_Msk   | 
                   SysTick_CTRL_ENABLE_Msk;  
  return (0); 
}

#endif



void SysTick_Delay_us(uint32_t us)//配置微妙时钟
{
	uint32_t i;
	SysTick_Config(72);
	
	for(i=0;i<us;i++)//循环一次一微妙
	{
		while( !((SysTick->CTRL) & (1<<16)) );//等待counter是否置1
	}
	
	SysTick->CTRL &=~ SysTick_CTRL_ENABLE_Msk;
}



void SysTick_Delay_ms(uint32_t ms)//配置毫秒时钟
{
	uint32_t i;
	SysTick_Config(72000);
	
	for(i=0;i<ms;i++)//循环一次一毫秒
	{
		while( !((SysTick->CTRL) & (1<<16)) );
	}
	
	SysTick->CTRL &=~ SysTick_CTRL_ENABLE_Msk;
}

