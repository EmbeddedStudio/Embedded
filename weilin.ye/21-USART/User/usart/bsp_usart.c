#include "bsp_usart.h"


//static void NVIC_Configuration(void)
//{
//	NVIC_InitTypeDef NVIC_InitStructure;
//	
//	/*嵌套向量中段控制器组选择*/
//	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
//	
//	/*配置USART为中断源*/
//	NVIC_InitStructure.NVIC_IRQChannel = DEBUG_USART_IRQ;
//	/*抢断优先级*/
//	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
//	/*子优先级*/
//	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
//	/*使能中断*/
//	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//	/*初始化配置NVIC*/
//	NVIC_Init(&NVIC_InitStructure);
//}


 void USART_Config(void)
{
	/*定义结构体*/
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;

	// 打开串口GPIO的时钟
	DEBUG_USART_GPIO_APBxClkCmd(DEBUG_USART_GPIO_CLK, ENABLE);
	
	// 打开串口1外设的时钟RCC_APB2Periph_USART1，串口2...为APB2
	DEBUG_USART_APBxClkCmd(DEBUG_USART_CLK, ENABLE);

	// 将USART Tx的GPIO配置为推挽复用模式
	GPIO_InitStructure.GPIO_Pin = DEBUG_USART_TX_GPIO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(DEBUG_USART_TX_GPIO_PORT, &GPIO_InitStructure);

  // 将USART Rx的GPIO配置为浮空输入模式
	GPIO_InitStructure.GPIO_Pin = DEBUG_USART_RX_GPIO_PIN;
	//外部发送什么电频就接收什么电频，浮空输出
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(DEBUG_USART_RX_GPIO_PORT, &GPIO_InitStructure);/*前面这部分是配置GPIO的*/
	
	// 配置串口的工作参数
	// 配置波特率115200
	USART_InitStructure.USART_BaudRate = DEBUG_USART_BAUDRATE;
	// 配置 针数据字长8位
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	// 配置停止位
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	// 配置校验位
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	// 配置硬件流控制，不使用
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	// 配置工作模式，收发一起
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	// 完成串口的初始化配置
	USART_Init(DEBUG_USARTx, &USART_InitStructure);
	
//	// 串口中断优先级配置
//	NVIC_Configuration();
//	
//	// 使能串口接收中断
//	USART_ITConfig(DEBUG_USARTx, USART_IT_RXNE, ENABLE);	
	
	// 使能串口
	USART_Cmd(DEBUG_USARTx, ENABLE);	    
}

//发送一个字节
void Usart_SendByte(USART_TypeDef* pUSARTx, uint8_t data)//pUSARTx中的p表示指针
{
	USART_SendData(pUSARTx,data);
	while(USART_GetFlagStatus(pUSARTx,USART_FLAG_TXE) == RESET);//如果不为0，表示为真，一个字节发送出去了
	
}


//发送两个字节，半个字，即16位
void Usart_SendHalfWord(USART_TypeDef* pUSARTx, uint16_t data)
{
	uint8_t temp_h,temp_1;
	
	temp_h = (data&0xff00) >> 8;//高八位
	temp_1 = data&0xff;//低八位
	
	//输出高八位
	USART_SendData(pUSARTx,temp_h);
	while(USART_GetFlagStatus(pUSARTx,USART_FLAG_TXE) == RESET);
	
	//输出低八位
	USART_SendData(pUSARTx,temp_1);
	while(USART_GetFlagStatus(pUSARTx,USART_FLAG_TXE) == RESET);
}


/*发送8位数据的数组array*/
void Usart_SendArray(USART_TypeDef* pUSARTx,uint8_t *array, uint8_t num)
{
	uint8_t i;
	for(i=0;i<num;i++)
	{
		Usart_SendByte(pUSARTx, *array++);//或者用Usart_SendByte(USART_TypeDef* pUSARTx, array[i])
		while(USART_GetFlagStatus(pUSARTx,USART_FLAG_TC) == RESET);//判断一连串的标志位为TC,一个字节的为TXE
	}
}


/*发送字符串*/
void Usart_SendStr(USART_TypeDef* pUSARTx,uint8_t *str)
{
	uint8_t i=0;
   do
	 {
		 Usart_SendByte(pUSARTx, *(str+i));
		 i++;
	 }while(*(str+i) != '\0');//遇到最后一个字符\0就停止输出,跳出循环
	 while(USART_GetFlagStatus(pUSARTx,USART_FLAG_TC) == RESET);
}

//重定向C库函数printf到串口，重定向后可使用printf函数
int fputc(int ch, FILE *f)//put char
{
	/*发送一个字节数据到串口*/
	USART_SendData(DEBUG_USARTx, (uint8_t) ch);
	
	/*等待发送完毕*/
	while(USART_GetFlagStatus(DEBUG_USARTx,USART_FLAG_TXE) == RESET);//RXNE是接收数据寄存器是否为空
	
	return (ch);
}

//重定向C库函数scanf到串口，重定向后可使用pscanf\getchar等函数
int fgetc(FILE *f)//get char
{
	/*等待串口数日数据*/
	while(USART_GetFlagStatus(DEBUG_USARTx,USART_FLAG_RXNE) == RESET);
	
	return (int)USART_ReceiveData(DEBUG_USARTx);
}
