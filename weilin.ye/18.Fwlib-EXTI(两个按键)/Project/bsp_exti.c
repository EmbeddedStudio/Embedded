#include "bsp_exti.h"


void EXIT_NVIC_Config(void)
{
	NVIC_InitTypeDef NVIC_InStruct;
	//配置中断优先级分组
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
	//配置中断源：按键1
	NVIC_InStruct.NVIC_IRQChannel = EXTI0_IRQn;
	//配置抢占优先级
	NVIC_InStruct.NVIC_IRQChannelPreemptionPriority = 1;
	//配置子优先级
	NVIC_InStruct.NVIC_IRQChannelSubPriority = 1;
	//使能中断通道
	NVIC_InStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InStruct);
	
	//配置中断源：按键2
	NVIC_InStruct.NVIC_IRQChannel = EXTI15_10_IRQn;
	NVIC_Init(&NVIC_InStruct);
	//其他配置和上面一样
}


void EXIT_Key_Config(void)
{
	GPIO_InitTypeDef   GPIO_InitStruct;
	EXTI_InitTypeDef   EXTI_InitStruct;
	
	//配置中断优先级
	EXIT_NVIC_Config();
	
//	//开启按键GPIO的时钟
//	RCC_APB2PeriphClockCmd(KEY1_GPIO_CLK,ENABLE);
//	GPIO_InitStruct.GPIO_Pin = KEY1_GPIO_PIN;
//	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN_FLOATING;
//	GPIO_Init(KEY1_GPIO_PORT ,&GPIO_InitStruct);
//	
//	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE);
//	GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource0);
//	
//	EXTI_InitStruct.EXTI_Line = EXTI_Line0;
//	EXTI_InitStruct.EXTI_Mode =  EXTI_Mode_Interrupt;
//	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;
//	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
//	EXTI_Init(&EXTI_InitStruct);
	
	//按键2
	RCC_APB2PeriphClockCmd(KEY2_GPIO_CLK,ENABLE);
	GPIO_InitStruct.GPIO_Pin = KEY2_GPIO_PIN;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(KEY2_GPIO_PORT ,&GPIO_InitStruct);
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE);
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOC,GPIO_PinSource13);
	
	EXTI_InitStruct.EXTI_Line = EXTI_Line13;
	EXTI_InitStruct.EXTI_Mode =  EXTI_Mode_Interrupt;
	EXTI_InitStruct.EXTI_Trigger =EXTI_Trigger_Falling;
	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStruct);
	
	
}

